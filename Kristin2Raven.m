function varargout = Kristin2Raven(varargin)
% FROM2RAVEN MATLAB code for Kristin2Raven.fig
%      FROM2RAVEN, by itself, creates a new FROM2RAVEN or raises the existing
%      singleton*.
%
%      H = FROM2RAVEN returns the handle to a new FROM2RAVEN or the handle to
%      the existing singleton*.
%
%      FROM2RAVEN('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in FROM2RAVEN.M with the given input arguments.
%
%      FROM2RAVEN('Property','Value',...) creates a new FROM2RAVEN or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before from2raven_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to from2raven_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Kristin2Raven

% Last Modified by GUIDE v2.5 19-Jun-2019 18:48:14

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @from2raven_OpeningFcn, ...
                   'gui_OutputFcn',  @from2raven_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before from2raven is made visible.
function from2raven_OpeningFcn(hObject, ~, handles, varargin)

    % Display icon
    icon = imread('Kermit-crop-48x48.png');
    imshow(icon);
    
    % Display version number
    handles.version.String = 'version 1.3';
    handles.version.Visible = 'on';
    handles.version.FontAngle = 'italic';

    % Choose default command line output for from2raven
    handles.output = hObject;

    % Update handles structure
    guidata(hObject, handles);

% UIWAIT makes from2raven wait for user response (see UIRESUME)
% uiwait(handles.mainFig);

% --- Outputs from this function are returned to the command line.
function varargout = from2raven_OutputFcn(hObject, ~, handles) 
% varargout  cell array for returning output args (see VARARGOUT);

    %Add application root directory to search paths
    addpath(fileparts(mfilename('fullpath')))

    % Get default command line output from handles structure
    varargout{1} = handles.output;

% --------------------------------------------------------------------
function frogLog_CreateFcn(hObject, ~,~)
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end

% --------------------------------------------------------------------
function frogLog_ButtonDownFcn(hObject, ~, handles)

    % Import Utilities
    import util.*

    %--
    % Find current path for input selection tables
    %--
    data = get(hObject,'UserData');
    if ~isempty(data)
        inpath = data.inpath;
    else
        inpath = pwd;
    end
    if ~isfolder(inpath)
        inpath = pwd;
    end
    
    %---
    % Set file type
    %---
    ext = 'xlsx';

    %--
    % Select selection tables for input
    %--
    filterSpect = fullfile(inpath, sprintf('*.%s',ext));
    [infile, inpath] = uigetfile( ...
        filterSpect, ...
        'Select frog log for input', ...
        'Multiselect', 'off');
    if isequal(infile, 0)
        return;
    end
    
    %---
    % Read coverage table
    %---
    infileFull = fullfile(inpath,infile);
    try
        % % [~, ~, C] = xlsread(truthSchedFullfile, sheet, '', 'basic');
        [~, ~, C] = xlsread(infileFull);
        frogLog = cell2mat(C(2:end,:));
                
        % Remove extra empty rows
        m = size(frogLog,1);
        idx = false(m,1);
        for i = 1:m
            idx(i) = all(isnan(frogLog(i,:)));
        end
        frogLog(idx,:) = [];
    catch
        dlg_name = 'Kristin2Raven';
        txt = sprintf('WARNING:\nFrog log could not be read:\n  "%s"',infile);
        fail(txt, dlg_name)
        return;
    end
    data.inpath = inpath;
% %     data.frogLog = C;
    data.frogLog = frogLog;
    set(hObject,'UserData',data);
    set(hObject,'String',infile);

% --------------------------------------------------------------------
function soundIn_CreateFcn(hObject, ~, ~)
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end

% --------------------------------------------------------------------
function soundIn_ButtonDownFcn(hObject, ~, handles)

    % Initializations
    import util.*

    %--
    % Find current path for input selection tables
    %--
    soundInPath = char(get(hObject,'UserData'));
    test = which('isfolder');
    if ~isempty(test)
        if ~isfolder(soundInPath)
            soundInPath = pwd;
        else
            if ~isdir(soundInPath)
                soundInPath = pwd;
            end
        end
    end
    
    %---
    % Set file type
    %---
    ext = '*';
    
    % Select root of file tree with selection tables
    if get(handles.fileTree,'Value')
        soundInPath = uigetdir(soundInPath, 'Select root of directory tree with sound files');
        if soundInPath == 0
            return;
        end
        h = waitbar(0, 'Finding files in tree.');
        soundInFull = fastDir(soundInPath, 'r', ext);
        [~,soundIn, ext] = cellfun(@fileparts, soundInFull, 'UniformOutput', false);
        soundIn = strcat(soundIn, ext);
        delete(h)
        set(hObject,'UserData',soundInPath);
        
    % Select invidual selection tables in a folder
    else

        %--
        % Select selection tables for input
        %--
        filterSpect = fullfile(soundInPath, sprintf('*.%s',ext));
        [soundIn, soundInPath] = uigetfile( ...
            filterSpect, ...
            'Select sound files for input', ...
            'Multiselect', 'on');
        if isequal(soundIn, 0)
            return;
        end
        set(hObject,'UserData',soundInPath);
    end
    if ~iscell(soundIn)
        soundIn = {soundIn};
    end
    
    % Display the names of selected sound files in UI
    set(hObject,'String',soundIn);

% --------------------------------------------------------------------
function fileTree_Callback(hObject, ~, handles)

    % Clear "Input Selection Tables" field
    handles.soundIn.String = {''};

% --------------------------------------------------------------------
function outpath_CreateFcn(hObject, ~, ~)
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end

% --------------------------------------------------------------------
function outpath_ButtonDownFcn(hObject, ~, handles)

    %--
    % Find current path
    %--
    outpath = char(handles.outpath.UserData);
    if ~isfolder(outpath)
        outpath = pwd;
    end

    %--
    % Select new input path
    %--
    outpath = uigetdir(outpath, 'Select folder for output');
    if isequal(outpath, 0)
        return;
    end
    set(hObject,'UserData',outpath);
    [~, outpathDisplay] = fileparts(outpath);
    set(hObject,'String',outpathDisplay);
    
% --------------------------------------------------------------------
function runButton_Callback(hObject, ~, handles)

    %----------------------------------------------------------------------
    % Initializations
    %----------------------------------------------------------------------
    wIDX = 0.2;
    h = waitbar(wIDX,'Starting ...','Name','Kristin2Raven');
    import util.*
    duration = 0.07; %event duration
    lowFreq = {'300'};
    hiFreq = {'3000'};
    header = {'Selection',...
              'View',...
              'Channel',...
              'Begin Time (s)',...
              'End Time (s)',...
              'Low Freq (Hz)',...
              'High Freq (Hz)',...
              'Begin Path',...
              'File Offset (s)',...
    };
    header2 = {'Selection',...
              'View',...
              'Channel',...
              'Begin Time (s)',...
              'End Time (s)',...
              'Low Freq (Hz)',...
              'High Freq (Hz)',...
    };
    view = {'Spectrogram 1'};
    
    % User Input
    frogLog = handles.frogLog.UserData.frogLog;
    t = frogLog(:,2:end)';
    [m,n] = size(t);
    fSound = handles.soundIn.String;
    dSound = fullfile(handles.soundIn.UserData, fSound);
    outpath = char(handles.outpath.UserData);
    serializedOutput = handles.serializedOutput.Value;
    multiplexedOutput = handles.multiplexedOutput.Value;
    multiplexSoundFiles = handles.multiplexSoundFiles.Value;
	singleOutput = handles.singleOutput.Value;
    if ~(singleOutput || serializedOutput || multiplexedOutput || multiplexSoundFiles)
        fail('Please select one or more output types')
        close(h)
        return;
    end
    
    % Paths
    soundPathMerged = fullfile(outpath,'multiplexed.wav');  %path with multiplexed sound file
    if multiplexSoundFiles && isfile(soundPathMerged)
        txt = 'A multiplexed sound file already exists on this path:';
        fail(sprintf('%s\n  "%s"',txt,soundPathMerged))
        close(h)
        return;
    end
    outPathFullMulti = fullfile(outpath,'multiplexed.selections.txt'); %path of multiplexed Raven selection table
    if multiplexedOutput && isfile(outPathFullMulti)
        txt = 'A multiplexed selection table file already exists on this path:';
        fail(sprintf('%s\n  "%s"',txt,outPathFullMulti))
        close(h)
        return;
    end
    outPathFullSerial = fullfile(outpath,'serialized.selections.txt'); %path of serialized Raven selection table
    if serializedOutput && isfile(outPathFullSerial)
        txt = 'A serialized selection table already exists on this path:';
        fail(sprintf('%s\n  "%s"',txt,outPathFullSerial))
        close(h)
        return;
    end
    if ~isequal(n,size(fSound,1))
        t1 = 'There are fewer records in frog log than number of sound files.';
        t2 = 'One or more sound files were ignored.';
        fail(sprintf('%s\n%s',t1,t2))
    end
        
    % Check that sample rate, bit depth, and number of channels is the same
    info = cellfun(@audioinfo,dSound);
    Fs = [info.SampleRate];
    bits = [info.BitsPerSample];
    numChannels = [info.NumChannels];
    assert(all(Fs==Fs(1)),...
        'All files must have the same sample rate');
    assert(all(bits==bits(1)),...
        'All files must have the same bit depth');
    assert(all(numChannels==numChannels(1)),...
        'All files must have the same number of channels');

    % Find cumulative duration of recordings before each sound file
    cumDur = cumsum([info.TotalSamples]) ./ Fs(1);
    
    % Set event times greater than file duration to zero
    disp(' ')
    soundDuration = [info.Duration];
    for i = 1:n
        outOfBounds = t(:,i)>soundDuration(i);
        t(outOfBounds,i) = 0;
        if any(outOfBounds)
            txt = 'WARNING: %.0f events have times greater than sound file duration in "%s"\n';
            fprintf(2,txt,sum(outOfBounds),fSound{i})
        end
    end
    
    %----------------------------------------------------------------------
    % Single Output
    %   one selection table per sound file
    %   (open sound files separately in Raven)
    %----------------------------------------------------------------------
    if singleOutput
        [~,d] = cellfun(@fileparts,fSound,'Unif',0);
        wIDX = wIDX + 0.2;
        waitbar(wIDX,h,'Creating a selection table for each sound file ...')
        outpathSingle = fullfile(outpath,'Separate Selection Tables'); %output path for single-channel selection tables
        if ~isfolder(outpathSingle)
            mkdir(outpathSingle)
        else
            if ~isempty(fastDir(outpathSingle))
                fail('"Separate Selection Tables" folder must be empty.')
                close(h)
                return;
            end
        end
        id = {'1';'2';'3';'4'};
        view2(1:n,1) = view;
        chan2(1:n,1) = {'1'};
        lowFreq2(1:n,1) = lowFreq;
        hiFreq2(1:n,1) = hiFreq;
        for i = 1:n
            beginTime = t(:,i);
            beginTime(beginTime==0) = [];
            beginTime = beginTime - duration./2;
            endTime = beginTime + duration;
            numEvents = size(beginTime,1);
            if numEvents>0
                dSoundCurr = dSound(i);
                dSoundCurr(1:n,1) = dSoundCurr;
                Cout = [header ;
                    id(1:numEvents,1),...
                    view2(1:numEvents,1),...
                    chan2(1:numEvents,1),...
                    cellstr(num2str(beginTime)),...
                    cellstr(num2str(endTime)),...
                    lowFreq2(1:numEvents,1),...
                    hiFreq2(1:numEvents,1),...
                    dSoundCurr(1:numEvents,1),...
                    cellstr(num2str(beginTime)),...
                ];
                outpathFullSingle = fullfile(outpathSingle,[d{i},'.selections.txt']);
                write_selection_table(Cout,outpathFullSingle);
            end
        end
    end
    
    %----------------------------------------------------------------------
    % Serialized Output
    %   one selection table for all sound files
    %   (open all sound files serially in Raven)
    %----------------------------------------------------------------------
    if serializedOutput
        wIDX = wIDX + 0.2;
        waitbar(wIDX,h,'Creating a selection table for serialized sound files ...')
        numEvents = m * n;
        chan2(1:numEvents,1) = {'1'};
        view(1:numEvents,1) = view;
        lowFreq(1:numEvents,1) = lowFreq;
        hiFreq(1:numEvents,1) = hiFreq;
        dSound3 = [];
        for i = 1:m
            dSound3 = [dSound3,dSound];
        end
        dSound3 = dSound3';        
        dSound3 = reshape(dSound3,[m*size(dSound,1),1]);
        dSound3 = dSound3(1:numEvents,1);
        beginTime = reshape(t,[numEvents,1]);
        beginTime = beginTime - duration./2;
        beginTime2 = migrateTime(beginTime,dSound3,dSound,cumDur);
        endTime = beginTime2 + duration;
        Cout = [strtrim(cellstr(num2str((1:numEvents)'))),...
                view,...
                chan2,...
                cellstr(num2str(beginTime2)),...
                cellstr(num2str(endTime)),...
                lowFreq,...
                hiFreq,...
                dSound3,...
                cellstr(num2str(beginTime)),...
        ];
        Cout(beginTime == -duration/2,:) = [];
        Cout = [header ; Cout];
        write_selection_table(Cout, outPathFullSerial)
    end
    
    %----------------------------------------------------------------------
    % Multiplexed Output
    %   one selection table for all sound files
    %   (open multiplexed sound file in Raven)
    %----------------------------------------------------------------------
    if multiplexedOutput
        
        %---
        % Make selection table
        %---
        wIDX = wIDX + 0.2;
        waitbar(wIDX,h,'Creating a selection table for multiplexed sound files ...')
        numEvents = m * n;
        beginTime = reshape(t,[numEvents,1]);
        beginTime = beginTime - duration./2;
        endTime = beginTime + duration;
        chan = [];
        for i = 1:m
            chan = [chan,frogLog(:,1)];
        end
        chan = chan';
        chan = reshape(chan,[numEvents,1]);
        view(1:numEvents,1) = view;
        lowFreq(1:numEvents,1) = lowFreq;
        hiFreq(1:numEvents,1) = hiFreq;
        dSound2(1:numEvents,1) = {soundPathMerged};
        Cout = [strtrim(cellstr(num2str((1:numEvents)'))),...
                view,...
                cellstr(num2str(chan)),...
                cellstr(num2str(beginTime)),...
                cellstr(num2str(endTime)),...
                lowFreq,...
                hiFreq,...
        ];
        Cout(beginTime == -duration/2,:) = [];
        Cout = [header2 ; Cout];
        write_selection_table(Cout, outPathFullMulti)
    end
    
    if multiplexSoundFiles
        
        %---
        % Multiplex sound files
        %---
        wIDX = wIDX + 0.2;
        waitbar(wIDX,h,'Multiplexing sound files ...')

        % Multiplex sound files
        X = [];
        for i = 1:length(dSound)
            X = [X,audioread(dSound{i})];
        end

        % Write multiplexed file
        outfile = fullfile(outpath,'multiplexed.wav');
        audiowrite(outfile,X,info(1).SampleRate);
    end

    % Finalize waitbar
    wIDX = 1;
    waitbar(wIDX,h,'Finished processing')

    
    
