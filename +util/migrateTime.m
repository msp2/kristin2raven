function t = migrateTime(file_offset,begin_file,listfile,cumDur)
%
% Migrate Raven selection table by updating Begin Time and End Time 
%   to make compatible with sound file stream.
%
% Inputs
%   file_offset: time vector in seconds (nx1 double)
%   begin_file: names of sound files for each event (nx1 cell array of strings)
%   listfile: names of sound files in listfile (nx1 cell array of strings)
%   cumDur: cumulative duration of sound files in stream (numeric vector)
%
% Output
%   t: migrated time vector in seconds (nx1 double)

%---
% Initializations
%---
import util.*

% % %---
% % % Set negative file offset to 0 file offset
% % %---
% % if any(file_offset < 0)
% %     file_offset(file_offset < 0) = 0;
% %     t(:,file_offset_idx) = cellstr(num2str(file_offset,'%.6f'));
% % end

%---
% Find number of seconds of recording before each file in stream
%---
if length( cumDur ) > 1
    time_before = [0, cumDur(1:end-1)];
else
    time_before = 0;
end

%---
% Calculate new "Begin Time" and "End Time" for each selection
%--
m = size(file_offset,1);
t = zeros( m, 1 );
% % new_end_time = t;
j = 0;
skipped_lines = [];
for i = 1:m
   
    % find begin file in sound file stream
    idx = strcmp( listfile, begin_file{ i } );
    
    % if no matching sound file found, skip selection
    if ~any( idx )
        skipped_lines = [ skipped_lines ; i ];
        fprintf(2,'WARNING:\nEvent deleted because Begin File not found in listfile:\n  %s\n\n',...
            begin_file{ i });
        continue;
    end
    
    % find number of seconds in file stream before begin file
    file_time = time_before( idx );
    
    % calculate new Begin Time
    curr_begin_time = file_time + file_offset( i );
% %     curr_end_time = curr_begin_time + diff( [ begin_time( i ), end_time( i ) ] );
    
    % put times in vector
    j = j + 1;
    t( j ) = curr_begin_time;
% %     new_end_time( j ) = curr_end_time;    
end

% trim skipped lines from "Begin Time" and "End Time" vectors
t = t( 1:j );
% % new_end_time = new_end_time( 1:j );

%---
% If any lines weren't skipped, replace "Begin Time" and "End Time" columns in table
%---
if ~isempty( t )
    
    % If any lines were skipped, delete them from output table
    if ~isempty( skipped_lines )
        t( skipped_lines, : ) = [];
    end
    
% %     % Update "Begin Time" and "End Time"
% %     new_begin_time_str = num2str( t, '%.6f' );
% %     new_begin_time_cell = strtrim(cellstr( new_begin_time_str ));
% %     t = new_begin_time_cell;
% % % %     new_end_time_str = num2str( new_end_time, '%.6f' );
% % % %     new_end_time_cell = strtrim(cellstr( new_end_time_str ));
% % % %     t( :, end_time_idx ) = new_end_time_cell;
% %     
% %     % Sort by Begin Time
% %     [~, sortIDX] = sort(t);
% %     t = t(sortIDX, :);

%---
% If all lines were skipped, table is empty
%---
else
    t = [];
end
